var gulp = require('gulp');
var stylus = require('gulp-stylus');
var autoprefixer = require('gulp-autoprefixer');


gulp.task('style', function () {
    return gulp.src('stylus/main.styl')
        .pipe(stylus())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: true
        }))
        .pipe(gulp.dest('css'))

});

gulp.task('default', ['style'], function () {
    gulp.watch('stylus/*.styl', ['style']);
});
